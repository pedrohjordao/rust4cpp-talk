---
theme: gaia
_class: lead
paginate: true
backgroundColor: #ffff
backgroundImage: url('https://marp.app/assets/hero-background.jpg')
footer: December 2019
marp: true
---

![w:500](assets/rustacean.svg)

# **What Rust Can Teach C++ Developers**

Pedro Jordão

---

# Presenter

Pedro Jordão

* Software Developer for 5 years
* C++, Rust and Fortran (:weary:) for fast simulation code
* JVM based languages for backend and GUI
* Currently working with the Foresight team @criticaltechworks with Scala and Apache Flink

---

# What This Talk Is

* A show of quirks of C++ and features of Rust
* A show of how Rust's compiler helps catch families of bugs
* A basic overview of tooling in Rust

---

# What This Talk Is Not

* A Rust tutorial (for that check out the Rust CoP Yammer!)
* A Full language review (I only have 15 minutes!)
* A "rewrite it in Rust" push 
    * But if you want to do that let's make a push!
* Talking bad about C++
    * I love C++!

---

# Quick Rust Intro

* Designed at Mozilla Research
* Low Level
* LLVM Based
* Strongly Typed
* Manual Memory Management Based on RAII
* Has Generics
* As Fast as C*

---

# Let's Play a Game

```cpp
// What is wrong with this code
void fun() {
    int* i;
    {
        int j = 0;
        i = &j;
    }
    std::cout << *i << '\n';
}
```
https://godbolt.org/z/KpzaZd

---

# How Rust Handles It

```rust
pub fn fun() {
    let i: &i32;
    {
        let j: i32 = 42;
        i = &j;
    }
    println!("{}", i)
}
```
https://godbolt.org/z/uUgEr5

---

# ...But C++ Is Learning

```cpp
void fun() {
    int* i;
    {
        int j = 0;
        i = &j;
    }
    std::cout << *i << '\n';
}
```
https://godbolt.org/z/NjKGTW

---

# Let's Play a Game

```cpp
// what is wrong here?
void fun() {
    vector<int> v;
    v.push_back(42);
    auto& i = v[0];
    v.push_back(666);
    std::cout << i << '\n';
}
```
https://godbolt.org/z/GtZxCY

---

# How Rust Handles It

```rust
pub fn fun() {
    let mut vec: Vec<i32> = Vec::new();
    vec.push(42);
    let i = &vec[0];
    vec.push(666);
    println!("{}", i)
}
```

https://godbolt.org/z/-JvMmc

---

# ...But C++ Is Learning

```cpp
void fun() {
    vector<int> v;
    v.push_back(42);
    auto& i = v[0];
    v.push_back(666);
    std::cout << i << '\n';
}
```
https://godbolt.org/z/Jcytvk

--- 

# Let's Play a Game

```cpp
class Account {
public:
    double balance = 10'000'000.0;

    bool withdraw(double amount) {
        double new_balance = balance - amount;
        if (new_balance < 0.0) {
            return false;
        } else {
            balance = new_balance;
            return true;
        }
    }
}:
```

---

# Let's Play a Game

```cpp
// what is wrong here?
void fun() {
    // main thread
    auto shared_account = make_shared<Account>();
    // ...
    // In many different threads
    shared_account->withdraw(100.0)
}
```
https://godbolt.org/z/ctJ6Zt

---

# Bonus Question

```cpp
// what is wrong here?
void fun() {
    // Thread I
    shared_ptr<A> a (new A (1));
    // Thread II
    shared_ptr<A> b (a);
    // Thread III
    shared_ptr<A> c (a);
    // Thread IV
    shared_ptr<A> d (a);
    d.reset (new A (10));
    // what is the value of a, b and c at this point?
}
```

---

# Let's Play a Game

Stolen from https://www.youtube.com/watch?v=3MB2iiCkGxg

* Is `shared_ptr` thread safe?
    * Yes?
    * No?
    * It depends?
    * Just as safe as a normal pointer?

---

# Let's Play a Game

![h:500](assets/shared_ptr_google.png)

---
# Let's Play a Game

![center h:480](assets/shared_ptr_structure.png)

---

# How Rust Handles It

```rust
struct Account {
    balance: f32
}
impl Account {
    fn new() -> Account {
        Account {
            balance: 10_000_000.0
        }
    }
    //...
```

---

# How Rust Handles It

```rust
    fn withdraw(&mut self, amount: f32) -> bool {
        let new_balance = self.balance - amount;
        if new_balance < 0.0 {
            false
        } else {
            self.balance = new_balance;
            true
        }
    }
}
```

---

# How Rust Handles It

```rust
fn main() {
    let shared_account = Arc::new(Account::new());
    let shared_account1 = shared_account.clone();
    let shared_account2 = shared_account.clone();
    thread::spawn(move || 
        while shared_account1.withdraw(100.0) {}
    );
    thread::spawn(move || 
        while shared_account2.withdraw(100.0) {}
    );
}
```
https://godbolt.org/z/ykJ4Sx

---

# Fixing it

```rust
fn main() {
    let shared_account = Arc::new(RwLock::new(Account::new()));
    let shared_account1 = shared_account.clone();
    let shared_account2 = shared_account.clone();
    thread::spawn(move || 
        while shared_account1.write().unwrap().withdraw(100.0) {}
    );
    thread::spawn(move || 
        while shared_account2.write().unwrap().withdraw(100.0) {}
    );
}
```
https://godbolt.org/z/bJ3w-N

---

# Conclusion

* Rust rules forces us to be correct
    * But you have escape hatches through `unsafe` if you know what you are doing (but you probably don't)
* Using Rust forces you to learn good practices that can be applied to C++
* The C++ compilers are learning, but mostly through experimental flags

---

# Conclusion

* Join us on Yammer @Rust CoP
* Let's build a JVM!
* Q&A